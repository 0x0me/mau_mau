defmodule MauMau.Engine.Context do
  @moduledoc """
  Overall game context holding:

  * the stack of undealt cards
  * a list of played cards
  * a list of players hands
  """
  defstruct stack: [], played: [], players: []

  @type context :: MauMau.Engine.Context

end
