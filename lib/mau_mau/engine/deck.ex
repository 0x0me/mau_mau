defmodule MauMau.Engine.Deck do
  @moduledoc """
  Simple 32 cards deck. To create a new unsuffled deck call
  ```elixir
  deck = MauMau.Engine.Deck.generate()
  ```

  or to create a shuffled deck

  ```elixir
  deck = MauMau.Engine.Deck.generate()
  |> Enum.shuffle
  ```
  """

  @shortdoc "Card deck"

  # suits
  @suits [:clubs, :spades, :hearts, :diamonds]

  # ranks
  @ranks [:seven, :eight, :nine, :ten, :jack, :queen, :king, :ace]

  @doc """
  Returns list of ranks. Defined ranks are
  ```elixir
  @ranks [:seven, :eight, :nine, :ten, :jack, :queen, :king, :ace]
  ```
  """
  def ranks, do: @ranks


  @doc """
  Returns list of suites. Defined suites are
  ```elixir
  @suits [:clubs, :spades, :hearts, :diamonds]
  ```
  """
  def suits, do: @suits

  @doc """
  Create a new deck.
  """
  def generate do
    for s <- @suits, r <- @ranks do
      {r, s}
    end
  end
end
