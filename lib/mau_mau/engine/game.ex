defmodule MauMau.Engine.Game do
  @moduledoc """
  Overall interface to control the game flow.
  """

  @cards 5

  @doc """
  Deal cards to players. Function expects an already shuffled deck.


  The number of cards defaults to 5 if there is no value passed in.
  ```elixir
  Enum.shuffe(deck)
  |> MauMau.Engine.Game.deal([:player1,:player2,:player3])
  ```

  Alternativly specify the number of cards which should be dealt to each player
  (default: 5)

  ```elixir
  Enum.shuffe(deck)
  |> MauMau.Engine.Game.deal([:player1,:player2,:player3], 6)
  ```
  returns a dictionary having the players as keys and the list of dealt cards as
  associated value.
  """
  def deal(deck, players, cards \\ @cards) do
    players
    |> Stream.cycle
    |> Enum.zip(Enum.take(deck, length(players) * cards))
    |> Enum.reduce(%{}, fn ({player, card}, acc) ->
      insert_or_append_value_list(acc, player, card)
    end)
  end

  # given a dict the function will either insert or update the value for a given
  # key
  defp insert_or_append_value_list(dict, key, value) do
    case Dict.get(dict, key) do
      nil ->
        # either it is empty -> insert
        Dict.put(dict, key, [value])
      old_value ->
        # or already populated -> append
        Dict.put(dict, key, [value | old_value])
    end
  end

  @doc """
  Create a game context from given and already shuffled card deck.

  ```
  MauMau.Engine.Deck.generate
  |> MauMau.Engine.Game.create_game([:player1,:player2, :player3], 7)
  ```
  """
  def create_game(deck, players, cards \\ @cards) do
    players_cards = deal(deck, players, cards)

    # convert dict to a list of dealt cards for calculating the remaining cards
    # on the stack
    dealt_cards = players_cards
    |> Dict.values
    |> Enum.flat_map(&(&1))

    stack = deck
    |> Enum.filter(fn (card) -> not Enum.member?(dealt_cards, card) end)

    # return game context
    %MauMau.Engine.Context{stack: stack,  players: players_cards,  played: []}
  end

  @doc """
  Play the _first_ card of a game for given player. Returning a modifed game
  context.
  """
  def play_card(context, player)  do
    play_card(context, player, &best_card/2)
  end

  @doc """
  Did not play any card for the player because of the previously played ace.

  """
  def play_card(context = %MauMau.Engine.Context{stack: _stack,  players: _players_cards,  played: [{:ace, _suit} | _tail]}, _player, _fun)  do
    # update context
    %MauMau.Engine.Context{context |
      played: [{:special, nil} | context.played] # this is the played card
    }
  end

  @doc """
  This method is invoked after an ace was played and the player before has
  already took its penalty. It took the played ace for card selection into
  account without triggering the effect again.
  """
  def play_card(context = %MauMau.Engine.Context{stack: _stack,  players: players_cards,  played: [{:special, _suit} | tail]}, player, fun)  do
    hand = players_cards[player]
    card = apply(fun, [hand, List.first(tail)])

    new_hand = List.delete(hand, card)

  # update context
    %MauMau.Engine.Context{context |
      players: Dict.put(players_cards, player, new_hand), # update players hand
      played: [card | context.played] # this is the played card
    }
  end


  @doc """
  Plays a card for given player taking the current card into account.

  Select a card from players hand using given function to determine the best
  card.
  """
  def play_card(context = %MauMau.Engine.Context{stack: _stack,  players: players_cards,  played: played}, player, fun)  do
    hand = players_cards[player]
    card = apply(fun, [hand, List.first(played)])

    new_hand = List.delete(hand, card)

    # update context
    %MauMau.Engine.Context{context |
      players: Dict.put(players_cards, player, new_hand), # update players hand
      played: [card | played] # this is the played card
    }
  end

  # select best card from given hand without considering any played cards
  #
  # This method is only useful for the player starting the game, because there
  # are no furter constraints which should be took into account at this moment
  def best_card(hand, card \\ nil)

  def best_card(hand, card = {rank, suit}) when is_tuple(card) do
    hand
    |> Enum.filter(fn ({r,s}) -> s == suit || r == rank  || r == :jack end)
    |> List.first
  end

  def best_card(hand, card) when is_nil(card) do
    # naive solution - just choose the first card from hand
    List.first(hand)
  end

end
