defmodule MauMau.Engine.GameTest do
  use ExUnit.Case

  test "deal five cards to three players" do
    players_cards = MauMau.Engine.Deck.generate
    |> MauMau.Engine.Game.deal([:player1, :player2, :player3])

    assert  length(players_cards[:player1]) === 5
    assert Enum.sort(players_cards[:player1]) === Enum.sort([{:seven, :clubs}, {:ten, :clubs}, {:king, :clubs}, {:eight, :spades}, {:jack, :spades}])

    assert  length(players_cards[:player2]) === 5
    assert Enum.sort(players_cards[:player2]) === Enum.sort([{:ace, :clubs}, {:eight, :clubs}, {:jack, :clubs}, {:nine, :spades}, {:queen, :spades}])

    assert  length(players_cards[:player3]) === 5
    assert Enum.sort(players_cards[:player3]) === Enum.sort([{:king, :spades}, {:nine, :clubs}, {:queen, :clubs}, {:seven, :spades}, {:ten, :spades}])
  end

  test "create a game context for three players holding three cards on game start" do
    context = MauMau.Engine.Deck.generate
    |> MauMau.Engine.Game.create_game([:player1, :player2, :player3], 3)

    players_cards = context.players

    assert length(players_cards[:player1]) === 3
    assert Enum.sort(players_cards[:player1]) === Enum.sort([{:seven, :clubs}, {:ten, :clubs}, {:king, :clubs}])

    assert  length(players_cards[:player2]) === 3
    assert Enum.sort(players_cards[:player2]) === Enum.sort([{:ace, :clubs}, {:eight, :clubs}, {:jack, :clubs}])

    assert  length(players_cards[:player3]) === 3
    assert Enum.sort(players_cards[:player3]) === Enum.sort([{:nine, :clubs}, {:queen, :clubs}, {:seven, :spades}])

    assert length(context.stack) === 23
    assert Enum.sort(context.stack) === Enum.sort([
      {:ace, :diamonds}, {:ace, :hearts}, {:ace, :spades}, {:eight, :diamonds},
      {:eight, :hearts}, {:eight, :spades}, {:jack, :diamonds}, {:jack, :hearts},
      {:jack, :spades}, {:king, :diamonds},{:king, :hearts}, {:king, :spades},
      {:nine, :diamonds}, {:nine, :hearts}, {:nine, :spades}, {:queen, :diamonds},
      {:queen, :hearts}, {:queen, :spades}, {:seven, :diamonds}, {:seven, :hearts},
      {:ten, :diamonds}, {:ten, :hearts}, {:ten, :spades}
    ])
  end

  test "play the first card for a player" do
    first_turn = MauMau.Engine.Deck.generate
    |> MauMau.Engine.Game.create_game([:player1, :player2, :player3], 3)
    |> MauMau.Engine.Game.play_card(:player2)

    assert length(first_turn.played) === 1
    assert first_turn.played === [{:ace, :clubs}]


    players_cards = first_turn.players

    assert length(players_cards[:player1]) === 3
    assert Enum.sort(players_cards[:player1]) === Enum.sort([{:seven, :clubs}, {:ten, :clubs}, {:king, :clubs}])

    assert  length(players_cards[:player2]) === 2
    assert Enum.sort(players_cards[:player2]) === Enum.sort([{:eight, :clubs}, {:jack, :clubs}])

    assert  length(players_cards[:player3]) === 3
    assert Enum.sort(players_cards[:player3]) === Enum.sort([{:nine, :clubs}, {:queen, :clubs}, {:seven, :spades}])

    assert length(first_turn.stack) === 23
    assert Enum.sort(first_turn.stack) === Enum.sort([
      {:ace, :diamonds}, {:ace, :hearts}, {:ace, :spades}, {:eight, :diamonds},
      {:eight, :hearts}, {:eight, :spades}, {:jack, :diamonds}, {:jack, :hearts},
      {:jack, :spades}, {:king, :diamonds},{:king, :hearts}, {:king, :spades},
      {:nine, :diamonds}, {:nine, :hearts}, {:nine, :spades}, {:queen, :diamonds},
      {:queen, :hearts}, {:queen, :spades}, {:seven, :diamonds}, {:seven, :hearts},
      {:ten, :diamonds}, {:ten, :hearts}, {:ten, :spades}
    ])
  end

  test "play a card matching to a given card" do
    context = MauMau.Engine.Deck.generate
    |> Enum.reverse
    |> MauMau.Engine.Game.create_game([:player1, :player2, :player3], 3)
    |> MauMau.Engine.Game.play_card(:player1)
    |> MauMau.Engine.Game.play_card(:player2, &MauMau.Engine.Game.best_card/2)
    |> MauMau.Engine.Game.play_card(:player3, &MauMau.Engine.Game.best_card/2)

    assert length(context.played) === 3
    assert context.played === [{:nine, :diamonds}, {:seven, :diamonds}, {:eight, :diamonds}]

    players_cards = context.players

    assert length(players_cards[:player1]) === 2
    assert Enum.sort(players_cards[:player1]) === Enum.sort([{:ace, :diamonds}, {:jack, :diamonds}])

    assert  length(players_cards[:player2]) === 2
    assert Enum.sort(players_cards[:player2]) === Enum.sort([{:king, :diamonds}, {:ten, :diamonds}])

    assert  length(players_cards[:player3]) === 2
    assert Enum.sort(players_cards[:player3]) === Enum.sort([{:ace, :hearts}, {:queen, :diamonds}])

    assert length(context.stack) === 23
    assert Enum.sort(context.stack) === Enum.sort([
      {:ace, :clubs}, {:ace, :spades}, {:eight, :clubs}, {:eight, :hearts},
      {:eight, :spades}, {:jack, :clubs}, {:jack, :hearts}, {:jack, :spades},
      {:king, :clubs}, {:king, :hearts}, {:king, :spades}, {:nine, :clubs},
      {:nine, :hearts}, {:nine, :spades}, {:queen, :clubs}, {:queen, :hearts},
      {:queen, :spades}, {:seven, :clubs}, {:seven, :hearts}, {:seven, :spades},
      {:ten, :clubs}, {:ten, :hearts}, {:ten, :spades}
    ])
  end


  test "ace is played" do
    context = MauMau.Engine.Deck.generate
    |> MauMau.Engine.Game.create_game([:player1, :player2, :player3], 3)
    |> MauMau.Engine.Game.play_card(:player1)
    |> MauMau.Engine.Game.play_card(:player2, &MauMau.Engine.Game.best_card/2)
    |> MauMau.Engine.Game.play_card(:player3, &MauMau.Engine.Game.best_card/2)
    |> MauMau.Engine.Game.play_card(:player1, &MauMau.Engine.Game.best_card/2)

    assert length(context.played) === 4
    assert context.played === [{:ten, :clubs},{:special, nil}, {:ace, :clubs},{:king, :clubs}]

    players_cards = context.players

    assert length(players_cards[:player1]) === 1
    assert Enum.sort(players_cards[:player1]) === Enum.sort([{:seven, :clubs}])

    assert  length(players_cards[:player2]) === 2
    assert Enum.sort(players_cards[:player2]) === Enum.sort([{:eight, :clubs}, {:jack, :clubs}])

    assert  length(players_cards[:player3]) === 3
    assert Enum.sort(players_cards[:player3]) === Enum.sort([{:nine, :clubs}, {:seven, :spades}, {:queen, :clubs}])

    assert length(context.stack) === 23
    assert Enum.sort(context.stack) === Enum.sort([
      {:ace, :diamonds}, {:ace, :hearts}, {:ace, :spades}, {:eight, :diamonds},
      {:eight, :hearts}, {:eight, :spades}, {:jack, :diamonds}, {:jack, :hearts},
      {:jack, :spades}, {:king, :diamonds},{:king, :hearts}, {:king, :spades},
      {:nine, :diamonds}, {:nine, :hearts}, {:nine, :spades}, {:queen, :diamonds},
      {:queen, :hearts}, {:queen, :spades}, {:seven, :diamonds}, {:seven, :hearts},
      {:ten, :diamonds}, {:ten, :hearts}, {:ten, :spades}
     ])
   end
end
