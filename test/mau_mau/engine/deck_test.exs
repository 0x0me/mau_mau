defmodule MauMau.Engine.DeckTest do
  use ExUnit.Case

  @expected_ordered_deck [
    seven: :clubs,
    eight: :clubs,
    nine: :clubs,
    ten: :clubs,
    jack: :clubs,
    queen: :clubs,
    king: :clubs,
    ace: :clubs,

    seven: :spades,
    eight: :spades,
    nine: :spades,
    ten: :spades,
    jack: :spades,
    queen: :spades,
    king: :spades,
    ace: :spades,

    seven: :hearts,
    eight: :hearts,
    nine: :hearts,
    ten: :hearts,
    jack: :hearts,
    queen: :hearts,
    king: :hearts,
    ace: :hearts,

    seven: :diamonds,
    eight: :diamonds,
    nine: :diamonds,
    ten: :diamonds,
    jack: :diamonds,
    queen: :diamonds,
    king: :diamonds,
    ace: :diamonds
  ]

  test "generate deck" do
    deck =  MauMau.Engine.Deck.generate

    assert @expected_ordered_deck |> Enum.count === deck |> Enum.count
    assert @expected_ordered_deck === deck
  end

  test "generate and shuffle deck" do
    shuffled = MauMau.Engine.Deck.generate |> Enum.shuffle
    assert @expected_ordered_deck |> Enum.count === shuffled |> Enum.count
    assert @expected_ordered_deck |> Enum.sort === shuffled |> Enum.sort
  end
end
