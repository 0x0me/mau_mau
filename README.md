# MauMau
An implementation of the "Mau Mau" game based on [Elixir](http://http://elixir-lang.org)
and [Phoenix](http://http://www.phoenixframework.org/).

## About Mau Mau
Mau Mau is a card game for 2 or more players. See wikipedia for more information
about [Mau Mau](https://en.wikipedia.org/wiki/Mau_mau_%28card_game%29). Each
player get (usually) 5 cards on the hand and has to try to get rid of them. If
one player cannot play a matching card, the player have to draw a card from the
stack.

# Build status

* Branch Develop: [![Build Status](https://semaphoreci.com/api/v1/0x0me/mau_mau/branches/develop/badge.svg)](https://semaphoreci.com/0x0me/mau_mau) [![Coverage Status](https://coveralls.io/repos/bitbucket/0x0me/mau_mau/badge.svg?branch=develop)](https://coveralls.io/bitbucket/0x0me/mau_mau?branch=develop)
